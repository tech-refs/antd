package main

import (
	"encoding/json"
	"log"
	"net/http"
)

// Params - calculation parameters
type Params struct {
	Num1 int `json:"num1"`
	Num2 int `json:"num2"`
}

func SumHandler(w http.ResponseWriter, r *http.Request) {
	var params Params
	err := json.NewDecoder(r.Body).Decode(&params)
	if err != nil {
		log.Print("Can't decode input params")
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

	result := params.Num1 + params.Num2
	bs, _ := json.Marshal(result)
	w.Header().Add("Access-Control-Allow-Origin", "*")
	w.Write(bs)
}
