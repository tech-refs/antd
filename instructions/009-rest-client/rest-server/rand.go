package main

import (
	"encoding/json"
	"math/rand"
	"net/http"
)

func RandomHandler(w http.ResponseWriter, r *http.Request) {
	min, max := 1, 100
	result := rand.Intn(max-min) + min
	bs, _ := json.Marshal(result)
	w.Header().Add("Access-Control-Allow-Origin", "*")
	w.Write(bs)
}
