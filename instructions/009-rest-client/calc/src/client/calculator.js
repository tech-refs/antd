// import request from 'axios';

// export async function queryCalculation(num1, num2) {
//   return request.post('/api/sum', { num1, num2 }).then(response => response.data);
// }

import request from '../utils/request';

export async function queryCalculation(num1, num2) {
  return request.post('/api/sum', { data: { num1, num2 } });
}
