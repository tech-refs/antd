// import request from 'axios';

// export async function queryRandom() {
//   return request('/api/rand').then(response => response.data);
// }

import request from '../utils/request';

export async function queryRandom() {
  return request('/api/rand');
}
