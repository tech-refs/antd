
# Instruction on antd ui + go rest server

# Run UI with mock

```sh
cd calc && \
yarn && \
yarn start
```

# Run UI with rest server

## Using local server

```sh
cd $GOPATH/src/gitlab.com/tech-refs/antd/instructions/009-rest-client/rest-server && \
go get ./... && \
go run main.go
```

## Using lambdas

```sh
cd rest-server
now
```

## Config proxy

To use local rest server or lambdas we need to set up proxy.
Add lines to /calc/config/config.js (specify server url at the target property)
```js
proxy: {
    '/api': {
      target: 'https://calc-go.sanctaignis.now.sh/',
      changeOrigin: true,
      pathRewrite: { '^/api' : '' }
    }
  },
```

## Run UI

```sh
cd calc && \
yarn && \
yarn start:no-mock
```
