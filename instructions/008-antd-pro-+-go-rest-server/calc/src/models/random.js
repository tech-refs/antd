import { query } from '@/services/random';

export default {
  namespace: 'random',
  state: 0,
  effects: {
    *rand(_, { call, put }) {
      const response = yield call(query);
      yield put({
        type: 'save',
        payload: response,
      });
    },
  },
  reducers: {
    save(_, { payload }) {
      return payload;
    },
  },
};
