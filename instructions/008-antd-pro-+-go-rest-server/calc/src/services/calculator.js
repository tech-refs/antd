import request from '@/utils/request';

export async function query(num1, num2) {
  return request.post('/api/sum', { data: { num1, num2 } });
}
