import React, { Component } from 'react';
import './App.css';
import Layout from 'antd/lib/layout';
import Menu from 'antd/lib/menu';
import Form from 'antd/lib/form';
import InputNumber from 'antd/lib/input-number';
import Button from 'antd/lib/button';
import Statistic from 'antd/lib/statistic';
import message from 'antd/lib/message';

class App extends Component {
  state = {
    current: 'calc',
  }

  handleClick = (e) => {
    this.setState({
      current: e.key,
    });
  }

  render() {
    return (
      <Layout>
        <Layout.Header>
          <Menu theme="dark" style={{ lineHeight: '64px' }} mode="horizontal" onClick={this.handleClick} selectedKeys={[this.state.current]}>
            <Menu.Item key="calc">Calc</Menu.Item>
            <Menu.Item key="random">Great Random</Menu.Item>
          </Menu>
        </Layout.Header>
        <Layout.Content style={{ padding: '20px 50px', margin: 'auto' }}>
          <Content page={this.state.current} />
        </Layout.Content>
      </Layout>
    );
  }
}

const Content = ({ page }) => {
  switch (page) {
    case 'calc':
      return <Calc />;
    case 'random':
      return <Random />;
    default:
      return null;
  }
}

class Calc extends React.Component {
  constructor(props) {
    super(props);
  }

  state = {
    num1: 0,
    num2: 0,
  }

  handleChange = (prop, value) => {
    this.setState({ [prop]: value });
  }

  handleSubmit = (e) => {
    e.preventDefault();
    message.success('Result of your calculation is: ' + (this.state.num1 + this.state.num2))
  }

  render() {
    return (
      <Form layout="inline" onSubmit={this.handleSubmit}>
        <Form.Item label="Num1">
          <InputNumber onChange={value => this.handleChange('num1', value)} />
        </Form.Item>
        <Form.Item label="+ Num2">
          <InputNumber onChange={value => this.handleChange('num2', value)} />
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit">Calculate</Button>
        </Form.Item>
      </Form>
    )
  }
}

class Random extends React.Component {
  constructor(props) {
    super(props);
  }

  state = {
    value: 0,
  }

  handleClick = () => {
    let min = 50;
    let max = 100;
    let rand = min - 0.5 + Math.random() * (max - min + 1)
    rand = Math.round(rand);
    
    this.setState({value: rand});
  }

  render() {
    return (
      <div>
        <Statistic title="Random number" value={this.state.value} precision={3} />
        <Button style={{ marginTop: 16 }} type="primary" onClick={this.handleClick}>Refresh</Button>
      </div>
    )
  }
}

export default App;
