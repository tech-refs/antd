
# Instruction on "Flow" installation

Copy files from previous step

```sh
cp -r ../009-rest-client/calc/ . && \
cd calc && \
yarn
```

Add Flow preset for babel

```sh
yarn add --dev @babel/preset-flow
```

Add preset to config/config.js

```js
extraBabelPresets: [
    "@babel/flow",
  ],
```

Install flow-bin

```sh
yarn add --dev flow-bin
```

Init flow

```sh
flow init
```

Add ignoring `.*/node_modules/.*` in `.flowconfig`

Check flow status

```sh
flow
```

Add `// @flow` to first lines of `src/client/calculator.js`, `src/client/index.js` and `src/client/random.js`

Check flow status again. There should be errors. Fix them.

```sh
flow
```

Check ui still works

```sh
yarn start
```

Add checking before commit in `package.json`

```json
"husky": {
    "hooks": {
      "pre-commit": "npm run prettier && npm run lint-staged && flow"
    }
  }
```