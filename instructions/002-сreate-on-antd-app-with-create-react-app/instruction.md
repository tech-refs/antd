
# Create on antd app with create-react-app

[Tutorial2](http://ant.design/docs/react/use-with-create-react-app)

## Create react app, navigate to its folder and start it

```sh
npm create react-app antd-demo && \
cd antd-demo
```

## Install antd

```sh
npm add antd
```

## Run app at current state

```sh
npm start
```

## Import antd css to the top of src/App.css

```css
@import '~antd/dist/antd.css';
```

Now we can import antd components to src/App.js and use it.   

```js
import Button from 'antd/lib/button';
```

And add button in header tag

```js
<div className="App">
    <Button type="primary">Button</Button>
</div>
```

Importing all styles in project could cause networking perfomance issue.

Stop app now for a while

## Install react-app-rewired, customize-cra and babel-plugin-import

```sh
npm add react-app-rewired customize-cra babel-plugin-import
```

React-app-rewired and customize-cra allows to override config.
Babel-plugin-import allows import components on demand.

Change "react-scripts" to "react-app-rewired" in the "scripts" field of package.json

## Create config-overrides.js at project root

```js
const { override, fixBabelImports } = require('customize-cra');

module.exports = override(
    fixBabelImports('import', {
        libraryName: 'antd',
        libraryDirectory: 'es',
        style: 'css',
    }),
);
```

Remove css import from src/App.css.  
Now we can import components like

```js
import { Button } from 'antd';
```

## Run app to check it is working

```sh
npm start
```

And then stop it

## Customize theme

Antd support [theme customizing](http://ant.design/docs/react/customize-theme)  
It allows to change colors, fonts and border styles with "less"

```sh
npm add less less-loader
```

Replace config-overrides.js with the code below to change primary color to green

```js
const { override, fixBabelImports, addLessLoader } = require('customize-cra');

module.exports = override(
    fixBabelImports('import', {
        libraryName: 'antd',
        libraryDirectory: 'es',
        style: true,
    }),
    addLessLoader({
        javascriptEnabled: true,
        modifyVars: { '@primary-color': '#1DA57A' },
    }),
);
```

## Check it again

```sh
npm start
```
