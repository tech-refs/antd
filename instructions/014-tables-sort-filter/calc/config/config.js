// ref: https://umijs.org/config/
import { primaryColor } from '../src/defaultSettings';

export default {
  plugins: [
    [
      'umi-plugin-react',
      {
        antd: true,
        dva: {
          hmr: true,
        },
        targets: {
          ie: 11,
        },
        locale: {
          enable: true, // default false
          default: 'en-US', // default en-US
          baseNavigator: true, // default true, when it is true, will use `navigator.language` overwrite default
        },
        dynamicImport: {
          loadingComponent: './components/PageLoading/index',
        },
      },
    ],
    [
      'umi-plugin-pro-block',
      {
        moveMock: false,
        moveService: false,
        modifyRequest: true,
        autoAddMenu: true,
      },
    ],
  ],
  targets: {
    ie: 11,
  },

  /**
   * Route related configuration
   */
  routes: [
    {
      path: '/',
      component: '../layouts/BasicLayout',
      Routes: ['src/pages/Authorized'],
      routes: [
        {
          path: '/',
          name: 'Catalog',
          icon: 'shopping',
          component: './Catalog',
        },
        {
          path: '/table',
          name: 'Catalog Table',
          icon: 'shopping',
          component: './CatalogTable',
        },
        {
          path: '/calc',
          name: 'Calculator',
          icon: 'calculator',
          component: './Calculator',
          authority: ['calculator', 'admin'],
        },
        {
          path: '/rand',
          name: 'Great random',
          icon: 'like',
          component: './Random',
        },
      ],
    },
  ],
  disableRedirectHoist: true,

  /**
   * webpack related configuration
   */
  define: {
    APP_TYPE: process.env.APP_TYPE || '',
  },
  proxy: {
    '/api': {
      target: 'http://localhost:9000/',
      // target: 'https://calc-go.sanctaignis.now.sh/',
      changeOrigin: true,
      pathRewrite: { '^/api': '' }
    }
  },
  /**
   * Theme for antd
   * https://ant.design/docs/react/customize-theme-cn
   */
  theme: {
    'primary-color': primaryColor,
  },
  externals: {
    '@antv/data-set': 'DataSet',
  },
  ignoreMomentLocale: true,
  lessLoaderOptions: {
    javascriptEnabled: true,
  },
  extraBabelPresets: [
    "@babel/flow",
  ],
};
