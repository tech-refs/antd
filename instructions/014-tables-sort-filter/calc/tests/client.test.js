// @flow
import { queryCalculation } from '../src/client';

describe('Calculation', () => {
  it('of 2 and 2 should be 4', () => {
    expect(queryCalculation(2, 2)).resolves.toBe(4);
  });
  it('of "string" and 2 should throw error', () => {
    expect(queryCalculation('string', 2)).rejects.toThrowError('incorrect incoming params');
  });
});