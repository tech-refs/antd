// @flow
import request from '../utils/request';

export async function queryCatalogIds(
  offset: number,
  portion: number,
  filterName: string,
  filterDescription: string,
  filterPriceLow: number,
  filterPriceHigh: number
) {
  if (typeof offset !== 'number' || typeof portion !== 'number') {
    throw new Error('incorrect incoming params');
  }
  return request(
    `api/?offset=${offset}&portion=${portion}&fname=${filterName}&fdesc=${filterDescription}&fpricel=${filterPriceLow}&fpriceh=${filterPriceHigh}`
  );
}

export async function queryCatalogItems(ids: number[]) {
  if (!Array.isArray(ids)) {
    throw new Error('incorrect incoming params');
  }
  return request(`api/items?id=${ids.toString()}`);
}
