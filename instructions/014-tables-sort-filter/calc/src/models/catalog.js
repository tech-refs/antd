import { queryIds, queryItems } from '@/services/catalog';
import { message } from 'antd';

export default {
  namespace: 'catalog',
  state: {
    offset: 0,
    portion: 10,
    hasMoreItems: true,
    items: new Map(),
  },
  effects: {
    *getNextPortion(_, { call, put, select }) {
      const state = yield select(({ catalog }) => catalog);
      if (state.hasMoreItems) {
        const { ids, lastPortion } = yield call(queryIds, state.offset, state.portion);
        const response = yield call(queryItems, ids);
        yield put({
          type: 'savePortion',
          payload: {
            items: response,
            hasMoreItems: !lastPortion,
            offset: state.offset + state.portion,
          },
        });
        if (lastPortion)
          message.success(`All downloaded. Total ${state.items.length + ids.length} elements`);
      }
    },
  },
  reducers: {
    savePortion(state, { payload }) {
      return {
        ...state,
        ...payload,
        items: new Map([...state.items, ...payload.items.map(item => [item.key, { ...item }])]),
      };
    },
  },
};
