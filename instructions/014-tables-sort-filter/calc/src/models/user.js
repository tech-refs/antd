import { getAuthority, login, handleAuthentication, logout } from '../services/auth0';
import { isAuthorizationCallbackURL } from '@/services/auth0';

const initialstate = {
  accessToken: '',
  idToken: '',
  expiresIn: null,
  idTokenPayload: null,
  unreadCount: 0,
};

export default {
  namespace: 'user',

  state: {
    ...initialstate,
  },

  effects: {
    *login() {
      yield login(window.location.href);
    },
    *logout(_, { call }) {
      const urlParams = new URL(window.location.href);
      yield call(logout, urlParams.origin);
    },
    *handleAuthentication({ payload }, { call, put }) {
      let authority;
      if (isAuthorizationCallbackURL(window.location.href)) {
        authority = yield call(handleAuthentication);
      } else {
        authority = yield call(getAuthority);
      }

      if (authority) {
        yield put({
          type: 'onLogin',
          payload: authority,
        });
      }
      yield put({
        type: 'menu/getMenuData',
        payload: { routes: payload.routes },
      });
    },
  },

  reducers: {
    save(state, action) {
      return {
        ...state,
        list: action.payload,
      };
    },
    onLogin(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
    onLogout(state) {
      return {
        ...state,
        ...initialstate,
      };
    },
    changeNotifyCount(state, action) {
      return {
        ...state,
        currentUser: {
          ...state.currentUser,
          notifyCount: action.payload.totalCount,
          unreadCount: action.payload.unreadCount,
        },
      };
    },
  },
};
