import { queryIds, queryItems } from '@/services/catalog';

const portion = 10;

export default {
  namespace: 'catalogTable',
  state: {
    offset: 0,
    hasMoreItems: true,
    items: [],
    filters: {},
    pagination: {},
    edgePrices: [],
  },
  effects: {
    *getNextPortion({ payload }, { call, put, select }) {
      const { pagination, filters } = payload;
      const state = yield select(({ catalogTable }) => catalogTable);
      const page = (pagination && pagination.current) || 1;
      const offset = (page - 1) * portion;

      const { ids, lastPortion, totalCount, minPrice, maxPrice } = yield call(
        queryIds,
        offset,
        portion,
        filters
      );
      const response = yield call(queryItems, ids);
      yield put({
        type: 'save',
        payload: {
          items: response,
          hasMoreItems: !lastPortion,
          offset: state.offset + state.portion,
          pagination: {
            total: totalCount,
            current: page,
          },
          filters,
          edgePrices: [minPrice, maxPrice],
        },
      });
    },
  },
  reducers: {
    save(state, { payload }) {
      return {
        ...state,
        ...payload,
      };
    },
  },
};
