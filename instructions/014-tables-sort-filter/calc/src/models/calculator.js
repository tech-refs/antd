import { query } from '@/services/calculator';
import { message } from 'antd';

export default {
  namespace: 'calc',
  state: {
    num1: 0,
    num2: 0,
  },
  effects: {
    *sum({ payload }, { call }) {
      const response = yield call(query, payload.num1, payload.num2);
      message.success(`Result of your calculation is: ${response}`);
    },
  },
  reducers: {
    onChange(state, { prop, value }) {
      return {
        ...state,
        [prop]: value,
      };
    },
  },
};
