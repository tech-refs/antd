import React from 'react';
import { connect } from 'dva';
import Catalog from '../components/CatalogTable';

const CatalogView = ({ dispatch, catalogTable }) => {
  function handleLoad() {
    dispatch({
      type: 'catalogTable/getNextPortion',
    });
  }
  function handleTableChange(pagination, filters, sorter) {
    dispatch({
      type: 'catalogTable/getNextPortion',
      payload: {
        pagination,
        filters,
        sorter,
      },
    });
  }

  return (
    <Catalog
      onLoadItems={handleLoad}
      onTableChange={handleTableChange}
      items={catalogTable.items}
      hasMoreItems={catalogTable.hasMoreItems}
      pagination={catalogTable.pagination}
      filters={catalogTable.filters}
      edgePrices={catalogTable.edgePrices}
    />
  );
};

export default connect(({ catalogTable }) => ({ catalogTable }))(CatalogView);
