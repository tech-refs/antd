import React from 'react';
import { List, Spin, Avatar } from 'antd';
import InfiniteScroll from 'react-infinite-scroller';

const loader = (
  <div key="-1" style={{ textAlign: 'center' }}>
    {' '}
    <Spin tip="Loading..." />
  </div>
);

const ListView = ({ onLoadItems, items, hasMoreItems }) => (
  <InfiniteScroll pageStart={0} loadMore={onLoadItems} hasMore={hasMoreItems} loader={loader}>
    <List
      dataSource={items}
      renderItem={(item, i) => (
        <List.Item key={i}>
          <List.Item.Meta
            avatar={<Avatar src={item.avatar} />}
            title={item.name}
            description={item.description}
          />
          <div>only {item.price}$</div>
        </List.Item>
      )}
    />
  </InfiniteScroll>
);

export default ListView;
