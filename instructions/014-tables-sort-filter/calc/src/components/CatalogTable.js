import React from 'react';
import { Avatar, Table, Input, Button, Icon, Slider } from 'antd';
import Highlighter from 'react-highlight-words';

class TableView extends React.PureComponent {
  componentDidMount() {
    const { items, onTableChange } = this.props;
    if (items.length === 0) onTableChange();
  }

  getColumnSearchProps = (dataIndex, { filters }) => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={node => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => confirm()}
          style={{ width: 188, marginBottom: 8, display: 'block' }}
        />
        <Button
          type="primary"
          onClick={() => confirm()}
          icon="search"
          size="small"
          style={{ width: 90, marginRight: 8 }}
        >
          Search
        </Button>
        <Button onClick={() => clearFilters()} size="small" style={{ width: 90 }}>
          Reset
        </Button>
      </div>
    ),
    filterIcon: filtered => (
      <Icon type="search" style={{ color: filtered ? '#1890ff' : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        .toString()
        .toLowerCase()
        .includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },
    render: text =>
      filters && filters[dataIndex] ? (
        <Highlighter
          highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
          searchWords={filters[dataIndex]}
          autoEscape
          textToHighlight={text}
        />
      ) : (
        text
      ),
    filteredValue: (filters && filters[dataIndex]) || '',
  });

  getPriceFilterProps = (dataIndex, { filters, edgePrices }) => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => {
      const min = (edgePrices && edgePrices[0]) || 0;
      const max = (edgePrices && edgePrices[1]) || 0;
      return (
        <div style={{ padding: 8 }}>
          <Slider
            range
            ref={node => {
              this.filterSlider = node;
            }}
            min={min}
            max={max}
            value={selectedKeys[0] || [min, max]}
            onChange={e => setSelectedKeys([e] || [])}
            style={{ width: 188, marginBottom: 8, display: 'block' }}
          />
          <Button
            type="primary"
            onClick={() => confirm()}
            icon="search"
            size="small"
            style={{ width: 90, marginRight: 8 }}
          >
            Search
          </Button>
          <Button onClick={() => clearFilters()} size="small" style={{ width: 90 }}>
            Reset
          </Button>
        </div>
      );
    },
    filterIcon: filtered => (
      <Icon type="filter" style={{ color: filtered ? '#1890ff' : undefined }} />
    ),
    onFilter: (value, record) => value[0] <= record[dataIndex] && record[dataIndex] <= value[1],
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => this.filterSlider.focus());
      }
    },
    filteredValue: (filters && filters[dataIndex]) || [],
  });

  render() {
    const { onTableChange, items, pagination, filters } = this.props;
    const columns = [
      {
        title: 'Avatar',
        dataIndex: 'avatar',
        key: 'avatar',
        width: 40,
        render: src => <Avatar src={src} />,
      },
      {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
        width: '10%',
        ...this.getColumnSearchProps('name', this.props),
      },
      {
        title: 'Description',
        dataIndex: 'description',
        key: 'description',
        ...this.getColumnSearchProps('description', this.props),
      },
      {
        title: 'Price',
        dataIndex: 'price',
        key: 'price',
        width: 130,
        render: text => `only ${text}$`,
        ...this.getPriceFilterProps('price', this.props),
      },
    ];
    return (
      <Table
        onChange={onTableChange}
        columns={columns}
        dataSource={items}
        pagination={pagination}
        filters={filters}
      />
    );
  }
}

export default TableView;
