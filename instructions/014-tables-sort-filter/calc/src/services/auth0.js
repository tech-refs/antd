import router from 'umi/router';
import auth0js from 'auth0-js';
import {
  getAuthority as utilsGetAuthority,
  setAuthority as utilsSetAuthority,
} from '../utils/authority';
import { reloadAuthorized } from '../utils/Authorized';
import { getFromLocalStorage, setToLocalStorage, nonce } from '../utils/utils';

export const auth0 = new auth0js.WebAuth({
  domain: 'sanctaignis.auth0.com',
  clientID: 'IinwJwl3FwbiMmpf5Qk8gpiZsWF90vZx',
  redirectUri: 'http://localhost:8000/',
  responseType: 'token id_token',
  scope: 'openid profile read:roles',
});

export function login(redirectPath) {
  const key = nonce();
  setToLocalStorage(key, { redirectPath });
  auth0.authorize({ state: key });
}

export function getAuthority() {
  return utilsGetAuthority();
}

export function setSession(authResult, redirectPath) {
  utilsSetAuthority(authResult);
  reloadAuthorized();

  // navigate to redirectPath or the home route
  const urlParams = new URL(window.location.href);
  router.push((redirectPath && redirectPath.replace(urlParams.origin, '')) || '/');
}

export function handleAuthentication() {
  return new Promise((resolve, reject) => {
    auth0.parseHash((err, authResult) => {
      if (authResult && authResult.accessToken && authResult.idToken) {
        const savedData = getFromLocalStorage(authResult.state);
        setSession(authResult, savedData ? savedData.redirectPath : '/');
        return resolve(authResult);
      }
      // console.log('Authentication error', err);
      return reject(err);
    });
  });
}

export function isAuthorizationCallbackURL(url) {
  if (typeof url !== 'string') {
    return false;
  }
  return url.includes('access_token');
}

export function logout(redirectPath) {
  return new Promise((resolve, reject) => {
    try {
      auth0.logout({
        returnTo: redirectPath,
      });
      setSession(null);
      resolve();
    } catch (err) {
      reject(err);
    }
  });
}

// renewSession() {
//   this.auth0.checkSession({}, (err, authResult) => {
//     if (authResult && authResult.accessToken && authResult.idToken) {
//       this.setSession(authResult);
//     } else if (err) {
//       this.logout();
//       console.log(err);
//       alert(`Could not get a new token (${err.error}: ${err.error_description}).`);
//     }
//   });
// }

// isAuthenticated() {
//   // Check whether the current time is past the
//   // access token's expiry time
//   let expiresAt = this.expiresAt;
//   return new Date().getTime() < expiresAt;
// }
// }
