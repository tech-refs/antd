import { queryCatalogItems, queryCatalogIds } from '../client';

export async function queryIds(offset, portion, filter) {
  const { name, description, price } = filter || {};
  const priceParams = (price && price[0]) || [0, 0];
  return queryCatalogIds(
    offset || 0,
    portion || 0,
    name || '',
    description || '',
    priceParams[0],
    priceParams[1]
  );
}

export async function queryItems(ids) {
  return queryCatalogItems(ids || []);
}
