import { getFromLocalStorage, setToLocalStorage } from './utils';

export function getAuthority() {
  return getFromLocalStorage('antd-pro-authority');
}

export function getAuthorityRoles() {
  const authority = getAuthority();
  if (authority && authority.idTokenPayload)
    return authority.idTokenPayload['http://localhost:8000/roles'] || undefined;
  return undefined;
}

export function setAuthority(authority) {
  return setToLocalStorage('antd-pro-authority', authority);
}
