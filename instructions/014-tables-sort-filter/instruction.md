
# Instruction on table filters

## Run backend

```sh
cd $GOPATH/src/gitlab.com/tech-refs/antd/instructions/014-tables-sort-filter/rest-server && \
go get ./... && \
go run main.go
```

## Run ui

```sh
cd calc
npm run start
```