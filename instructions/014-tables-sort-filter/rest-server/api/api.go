package api

import (
	"encoding/json"
	"log"
	"math/rand"
	"net/http"
	"time"
)

// ListItem -
type ListItem struct {
	Key         int    `json:"key"`
	Name        string `json:"name"`
	Avatar      string `json:"avatar"`
	Description string `json:"description"`
	Price       int    `json:"price"`
}

// ListItems -
type ListItems []*ListItem

// Filter -
func (list *ListItems) Filter(f func(*ListItem) bool) *ListItems {
	filteredList := make(ListItems, 0)
	for _, item := range *list {
		if f(item) {
			filteredList = append(filteredList, item)
		}
	}
	return &filteredList
}

// Keys -
func (list *ListItems) Keys() *[]int {
	l := *list
	keys := make([]int, len(l))
	for index, item := range l {
		keys[index] = item.Key
	}
	return &keys
}

// Slice -
func (list *ListItems) Slice(start, end int) *ListItems {
	part := (*list)[start:end]
	return &part
}

// Select -
func (list *ListItems) Select(ids *[]int) *ListItems {
	items := ListItems{}
	for _, item := range *list {
		if sliceContains(ids, item.Key) {
			items = append(items, item)
		}
	}
	return &items
}

func sliceContains(slice *[]int, id int) bool {
	for _, elem := range *slice {
		if elem == id {
			return true
		}
	}
	return false
}

// EdgePrices -
func (list *ListItems) EdgePrices() (min, max int) {
	l := *list
	if len(l) > 0 {
		min, max = l[0].Price, l[0].Price
	}
	for _, item := range l {
		if item.Price < min {
			min = item.Price
		}
		if item.Price > max {
			max = item.Price
		}
	}
	return
}

// GetData -
func GetData() *ListItems {
	resp, err := http.Get("http://names.drycodes.com/100?nameOptions=objects&separator=space&format=json")
	if err != nil {
		log.Fatal(err)
	}
	var names []string
	err = json.NewDecoder(resp.Body).Decode(&names)
	if err != nil {
		log.Fatal(err)
	}
	data := make(ListItems, len(names))
	for index, name := range names {
		data[index] = generateListItem(name, index)
	}
	return &data
}

func generateListItem(name string, index int) *ListItem {
	seed := rand.NewSource(time.Now().UnixNano())
	random := rand.New(seed)
	return &ListItem{
		Key:         index,
		Name:        name,
		Avatar:      "https://img.icons8.com/metro/420/github.png",
		Description: "That's beautifull " + name + " we have all types of " + name + " thousands people around the world using " + name + " every day",
		Price:       random.Intn(2000) + 100,
	}
}
