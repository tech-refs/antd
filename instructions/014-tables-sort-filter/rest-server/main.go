package main

import (
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"strings"
	"time"

	"github.com/gorilla/schema"

	"github.com/gorilla/mux"
	"gitlab.com/tech-refs/antd/instructions/014-tables-sort-filter/rest-server/api"
)

type responseData struct {
	Ids         []int `json:"ids"`
	TotalCount  int   `json:"totalCount"`
	LastPortion bool  `json:"lastPortion"`
	MinPrice    int   `json:"minPrice"`
	MaxPrice    int   `json:"maxPrice"`
}

type querySearchParams struct {
	Portion           int    `schema:"portion"`
	Offset            int    `schema:"offset"`
	FilterName        string `schema:"fname"`
	FilterDescription string `schema:"fdesc"`
	FilterPriceLow    int    `schema:"fpricel"`
	FilterPriceHigh   int    `schema:"fpriceh"`
}

type querySelectParams struct {
	Keys []int `schema:"id"`
}

const baseURL = "localhost"
const port string = "9000"

var data *api.ListItems

func main() {
	fmt.Printf("Starting server at %v port", port)
	data = api.GetData()
	router := mux.NewRouter()
	router.HandleFunc("/", catalogIds).Methods("GET")
	router.HandleFunc("/items", catalogItems).Methods("GET")
	log.Fatal(http.ListenAndServe(baseURL+":"+port, router))
}

func getQuerySearchParams(r *http.Request) *querySearchParams {
	params := &querySearchParams{}
	decoder := schema.NewDecoder()
	err := decoder.Decode(params, r.URL.Query())
	if err != nil {
		fmt.Println(err)
	}

	return params
}

func getQuerySelectParams(r *http.Request) *querySelectParams {
	params := &querySelectParams{}
	decoder := schema.NewDecoder()
	err := decoder.Decode(params, r.URL.Query())
	if err != nil {
		fmt.Println(err)
	}

	return params
}

func catalogIds(w http.ResponseWriter, r *http.Request) {
	time.Sleep(200 * time.Millisecond)
	params := getQuerySearchParams(r)

	filteredData := data.Filter(func(item *api.ListItem) bool {
		if params.FilterName == "" {
			return true
		}
		return strings.Contains(strings.ToLower(item.Name), strings.ToLower(params.FilterName))
	})
	filteredData = filteredData.Filter(func(item *api.ListItem) bool {
		if params.FilterDescription == "" {
			return true
		}
		return strings.Contains(strings.ToLower(item.Description), strings.ToLower(params.FilterDescription))
	})
	filteredData = filteredData.Filter(func(item *api.ListItem) bool {
		if params.FilterPriceLow >= params.FilterPriceHigh || params.FilterPriceHigh == 0 {
			return true
		}
		return params.FilterPriceLow <= item.Price && item.Price <= params.FilterPriceHigh
	})

	upperBound := params.Offset + params.Portion
	length := len(*filteredData)
	if upperBound > length || params.Portion == 0 {
		upperBound = length
	}
	part := filteredData.Slice(params.Offset, upperBound)
	minPrice, maxPrice := data.EdgePrices()
	writeResponse(w, responseData{
		Ids:         *part.Keys(),
		TotalCount:  length,
		LastPortion: length <= params.Offset+params.Portion,
		MinPrice:    minPrice,
		MaxPrice:    maxPrice,
	})
}

func catalogItems(w http.ResponseWriter, r *http.Request) {
	time.Sleep(500 * time.Millisecond)
	params := getQuerySelectParams(r)
	items := data.Select(&params.Keys)
	writeResponse(w, items)
}

// random - responses random int from 1 to 100
func random(w http.ResponseWriter, r *http.Request) {
	min, max := 1, 100
	result := rand.Intn(max-min) + min
	writeResponse(w, result)
}

func addOriginHeader(w http.ResponseWriter) {
	headers := w.Header()
	headers.Add("Access-Control-Allow-Origin", "*")
}

func writeResponse(w http.ResponseWriter, data interface{}) {
	bs, _ := json.Marshal(data)
	addOriginHeader(w)
	w.Write(bs)
}
