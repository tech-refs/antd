/* eslint no-useless-escape:0 import/prefer-default-export:0 */
import { parse } from 'qs';
import { randomBytes } from 'crypto';

const reg = /(((^https?:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+(?::\d+)?|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)$/;

export function isUrl(path) {
  return reg.test(path);
}

export function getPageQuery() {
  return parse(window.location.href.split('?')[1]);
}

export function nonce() {
  return randomBytes(16).toString('base64');
}

export function getFromLocalStorage(key) {
  if (!key || typeof key !== 'string') return {};
  const dataString = localStorage.getItem(key);
  let data;
  try {
    data = JSON.parse(dataString);
  } catch (e) {
    data = {};
  }
  return data;
}

export function setToLocalStorage(key, value) {
  return localStorage.setItem(key, JSON.stringify(value));
}
