export default {
  'menu.Calculator': 'Calculator',
  'menu.Great random': 'Great Random',
  'menu.Catalog': 'Catalog',

  'menu.account.center': 'Account Center',
  'menu.account.settings': 'Account Settings',
  'menu.account.trigger': 'Trigger Error',
  'menu.account.logout': 'Logout',
};
