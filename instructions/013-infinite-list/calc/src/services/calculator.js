import { queryCalculation } from '../client';

export async function query(num1, num2) {
  return queryCalculation(num1, num2);
}
