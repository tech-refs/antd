import { queryCatalogItems } from '../client';

export async function query(offset, portion) {
  return queryCatalogItems(offset, portion);
}
