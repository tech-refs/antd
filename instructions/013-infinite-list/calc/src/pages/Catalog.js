import React from 'react';
import { connect } from 'dva';
import Catalog from '../components/Catalog';

const CatalogView = ({ dispatch, catalog }) => {
  function handleLoad() {
    dispatch({
      type: 'catalog/getNextPortion',
    });
  }
  return (
    <Catalog onLoadItems={handleLoad} items={catalog.items} hasMoreItems={catalog.hasMoreItems} />
  );
};

export default connect(({ catalog }) => ({ catalog }))(CatalogView);
