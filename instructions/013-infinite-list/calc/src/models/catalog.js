import { query } from '@/services/catalog';
import { message } from 'antd';

export default {
  namespace: 'catalog',
  state: {
    offset: 0,
    portion: 10,
    hasMoreItems: true,
    items: [],
  },
  effects: {
    *getNextPortion(_, { call, put, select }) {
      const state = yield select(({ catalog }) => catalog);
      const response = yield call(query, state.offset, state.portion);
      yield put({
        type: 'save',
        payload: {
          items: response.items,
          hasMoreItems: !response.lastPortion,
          offset: state.offset + state.portion,
        },
      });

      if (response.lastPortion)
        message.success(
          `All downloaded. Total ${state.items.length + response.items.length} elements`
        );
    },
  },
  reducers: {
    save(state, { payload }) {
      const items = [...state.items, ...payload.items];
      return {
        ...state,
        ...payload,
        items,
      };
    },
  },
};
