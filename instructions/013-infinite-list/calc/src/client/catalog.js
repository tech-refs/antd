// @flow
import request from '../utils/request';

export async function queryCatalogItems(offset: number, portion: number) {
  if (typeof offset !== 'number' || typeof portion !== 'number') {
    throw new Error('incorrect incoming params');
  }
  return request(`http://localhost:9000/?offset=${offset}&portion=${portion}`, {
    credentials: 'omit',
  });
}
