// @flow
export async function queryCalculation(num1: number, num2: number) {
  if (typeof num1 !== 'number' || typeof num2 !== 'number') {
    throw new Error('incorrect incoming params');
  }
  return num1 + num2;
}
