
# Instruction on infinite scroll

## React infinite scroll

Infinite scroll needs "react-infinite-scroll" package to work

```sh
npm install react-infinite-scroller --save
```

## Run backend

```sh
cd $GOPATH/src/gitlab.com/tech-refs/antd/instructions/013-infinite-list/rest-server && \
go get ./... && \
go run main.go
```

## Run ui

```sh
cd calc
npm run start
```