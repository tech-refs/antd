package main

import (
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
)

type listItem struct {
	Name        string `json:"name"`
	Avatar      string `json:"avatar"`
	Description string `json:"description"`
	Price       int    `json:"price"`
}
type listItems struct {
	Items       []listItem `json:"items"`
	LastPortion bool       `json:"lastPortion"`
}

const baseURL = "localhost"
const port string = "9000"

var names []string

func main() {
	fmt.Printf("Starting server at %v port", port)
	getNames()
	router := mux.NewRouter()
	router.HandleFunc("/", catalogList).Methods("GET")
	log.Fatal(http.ListenAndServe(baseURL+":"+port, router))
}

// GET http://names.drycodes.com/100?nameOptions=objects&separator=space&format=json
func getNames() {
	resp, err := http.Get("http://names.drycodes.com/100?nameOptions=objects&separator=space&format=json")
	if err != nil {
		log.Fatal(err)
	}
	err = json.NewDecoder(resp.Body).Decode(&names)
	if err != nil {
		log.Fatal(err)
	}
}

func catalogList(w http.ResponseWriter, r *http.Request) {
	time.Sleep(700 * time.Millisecond)
	queryParams := r.URL.Query()
	portionParam, ok := queryParams["portion"]
	portion := 0
	if ok && len(portionParam[0]) > 0 {
		portion, _ = strconv.Atoi(portionParam[0])
	}
	_ = portion

	offsetParam, ok := queryParams["offset"]
	offset := 0
	if ok && len(offsetParam[0]) > 0 {
		offset, _ = strconv.Atoi(offsetParam[0])
	}
	_ = offset

	upperBound := offset + portion
	length := len(names)
	if upperBound > length {
		upperBound = length
	}

	listItems := listItems{}
	seed := rand.NewSource(time.Now().UnixNano())
	random := rand.New(seed)
	namesPortion := names[offset:upperBound]
	for _, name := range namesPortion {
		item := listItem{
			Name:        name,
			Avatar:      "https://img.icons8.com/metro/420/github.png",
			Description: "That's beautifull " + name + " we have all types of " + name + " thousands people around the world using " + name + " every day",
			Price:       random.Intn(2000) + 100,
		}
		listItems.Items = append(listItems.Items, item)
	}
	listItems.LastPortion = length <= offset+portion

	bs, _ := json.Marshal(listItems)
	addOriginHeader(w)
	w.Write(bs)
}

// random - responses random int from 1 to 100
func random(w http.ResponseWriter, r *http.Request) {
	min, max := 1, 100
	result := rand.Intn(max-min) + min
	bs, _ := json.Marshal(result)
	addOriginHeader(w)
	w.Write(bs)
}

func addOriginHeader(w http.ResponseWriter) {
	headers := w.Header()
	headers.Add("Access-Control-Allow-Origin", "*")
}
