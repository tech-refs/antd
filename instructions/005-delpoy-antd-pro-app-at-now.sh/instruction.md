
# Delpoy react app at zeit.co (now.sh)

## Install "now" CLI

```sh
yarn global add now
```

For this tutorial will be used tutrial3 sources.

## Copy app from tutorial 3

```sh
cp -r ../tutorial3/calc .
```

## Navigate to app folder and install dependencies

```sh
cd calc && \
yarn
```

## Create now.json with the following content

```json
cat > now.json << EOF
{
    "version": 2,
    "name": "calc",
    "builds": [
        {
            "src": "package.json",
            "use": "@now/static-build",
            "config": {
                "distDir": "build"
            }
        }
    ]
}
EOF
```

## Execute "now" from CLI
It will register you if it is first time of using now.sh. In that case you should execute command second time

```sh
now
```