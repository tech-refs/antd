package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"

	"github.com/gorilla/mux"
)

// Params - calculation parameters
type Params struct {
	Num1 int `json:"num1"`
	Num2 int `json:"num2"`
}

func main() {
	fmt.Printf("Starting server at 9000 port")
	router := mux.NewRouter()
	router.HandleFunc("/", help).Methods("GET")
	router.HandleFunc("/sum", calculate).Methods("POST")
	router.HandleFunc("/rand", random).Methods("GET")
	log.Fatal(http.ListenAndServe(":9000", router))
}

// help - help contents
func help(w http.ResponseWriter, r *http.Request) {
	message, err := ioutil.ReadFile("help.html")
	if err != nil {
		log.Print("Can't read help.html")
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
	addOriginHeader(w)
	w.Write(message)
}

// calculate - adds num2 to num1
func calculate(w http.ResponseWriter, r *http.Request) {
	var params Params
	err := json.NewDecoder(r.Body).Decode(&params)
	if err != nil {
		log.Print("Can't decode input params")
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

	result := params.Num1 + params.Num2
	bs, _ := json.Marshal(result)
	addOriginHeader(w)
	w.Write(bs)
}

// random - responses random int from 1 to 100
func random(w http.ResponseWriter, r *http.Request) {
	min, max := 1, 100
	result := rand.Intn(max-min) + min
	bs, _ := json.Marshal(result)
	addOriginHeader(w)
	w.Write(bs)
}

func addOriginHeader(w http.ResponseWriter) {
	headers := w.Header()
	headers.Add("Access-Control-Allow-Origin", "*")
}
