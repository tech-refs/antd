
# Instruction on start restfull server


## Install dependencies and run

```sh
cd $GOPATH/src/gitlab.com/tech-refs/antd/instructions/007-go-rest && \
go get ./... && \
go run main.go
```

## Check sum

```sh
curl -X POST localhost:9000/sum --data '{"num1":1,"num2":2}'
```

## Check rand

```sh
curl localhost:9000/rand
```
