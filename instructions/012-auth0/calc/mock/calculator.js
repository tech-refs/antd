export default {
    'POST /api/sum.go': (req, res) => {
        const { num1, num2 } = req.body;
        res.send(JSON.stringify(num1 + num2));
    },
}