import { queryCalculation } from './calculator';
import { queryRandom } from './random';

export { queryCalculation, queryRandom };
