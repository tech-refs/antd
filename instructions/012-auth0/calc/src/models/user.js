import { getAuthority, login, handleAuthentication, logout } from '../services/auth0';

const initialstate = {
  accessToken: '',
  idToken: '',
  expiresIn: null,
  idTokenPayload: null,
  unreadCount: 0,
};

export default {
  namespace: 'user',

  state: {
    ...initialstate,
  },

  effects: {
    *fetchCurrent(_, { call, put }) {
      const authority = yield call(getAuthority);
      if (authority) {
        yield put({
          type: 'onLogin',
          payload: authority,
        });
      }
    },
    *login() {
      yield login(window.location.href);
    },
    *logout(_, { call }) {
      const urlParams = new URL(window.location.href);
      yield call(logout, urlParams.origin);
    },
    *handleAuthentication(_, { call, put }) {
      const authority = yield call(handleAuthentication);
      if (authority) {
        yield put({
          type: 'onLogin',
          payload: authority,
        });
      }
    },
  },

  reducers: {
    save(state, action) {
      return {
        ...state,
        list: action.payload,
      };
    },
    onLogin(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
    onLogout(state) {
      return {
        ...state,
        ...initialstate,
      };
    },
    changeNotifyCount(state, action) {
      return {
        ...state,
        currentUser: {
          ...state.currentUser,
          notifyCount: action.payload.totalCount,
          unreadCount: action.payload.unreadCount,
        },
      };
    },
  },
};
