import React from 'react';
import { connect } from 'dva';
import Calculator from '../components/Calculator';

const Calc = ({ dispatch, calc }) => {
  function handleChange(prop, value) {
    dispatch({
      type: 'calc/onChange',
      prop,
      value,
    });
  }
  function handleSubmit(num1, num2) {
    dispatch({
      type: 'calc/sum',
      payload: { num1, num2 },
    });
  }

  return (
    <Calculator onSubmit={handleSubmit} onChange={handleChange} num1={calc.num1} num2={calc.num2} />
  );
};

export default connect(({ calc }) => ({ calc }))(Calc);
