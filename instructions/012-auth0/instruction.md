
# Instruction on setting up Auth0

## Add roles to token payload

At the Auth0 dashboard: Rules -> Create rule.  
Insert the following JS code:

```js
function (user, context, callback) {
  const assignedRoles = (context.authorization || {}).roles;
  let idTokenClaims = context.idToken || {};
  
  idTokenClaims['http://localhost:8000/roles'] = assignedRoles;
  
  context.idToken = idTokenClaims;
  callback(null, user, context);
}
```

"http://localhost:8000/roles" - is arbitrary namespace

## Create roles and assign them to users

At the Auth0 dashboard: Users & Roles -> Roles -> Create role.  
Roles names must be equal to application authority names used at routes config and "Authorized" component.

## Permission routing

Permission routing allows check authority before routing and hide corresponding menu items.

Following example grants access for "admin" and "calculator" roles to "Calculator" route.

[config.js](calc/config/config.js):

```js
{
    path: '/',
    name: 'Calculator',
    icon: 'calculator',
    component: './Calculator',
    authority: ['calculator', 'admin'],
}
```

## "Authorized" component

Allows to hide child components enclosed in the "Authorized" component.

[Calculator.js](calc/src/components/Calculator.js):

```js
import Authorized from '@/utils/Authorized';

const noMatch = <p>Calculate button available only for admins</p>;

<Authorized authority="admin" noMatch={noMatch}>
    <Button type="primary" htmlType="submit">
        Calculate
    </Button>
</Authorized>
```