
# Antd references

[Create umi app](001-сreate-umi-app/instruction.md)

[Create on antd app with create-react-app](002-сreate-on-antd-app-with-create-react-app/instruction.md)

[2 pages, simple sum and random numeber](003-2-pages,-simple-sum-and-random-numeber/instruction.md)

[Antd on codesandbox.io](004-antd-on-codesandbox.io.md)

[Delpoy antd pro app at now.sh](005-delpoy-antd-pro-app-at-now.sh/instruction.md)

[Delpoy from codesandbox.io to now.sh](006-delpoy-from-codesandbox.io-to-now.sh.md)

[Restfull server on go](007-go-rest/instruction.md)

[Antd pro + go rest server](008-antd-pro-+-go-rest-server/instruction.md)

[Rest client](009-rest-client/instruction.md)

[Rest client on flow](010-rest-client-on-flow/instruction.md)
