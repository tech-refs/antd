export default {
    'GET /api/rand.go': (req, res) => {
        const min = 50;
        const max = 100;
        const rand = min - 0.5 + Math.random() * (max - min + 1)
        const value = Math.round(rand);
        res.send(JSON.stringify(value));
    },
}