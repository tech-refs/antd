import React from 'react';
import { Form, InputNumber, Button } from 'antd';

const Calculator = ({ onSubmit, onChange, num1, num2 }) => (
  <Form
    layout="inline"
    onSubmit={e => {
      e.preventDefault();
      onSubmit(num1, num2);
    }}
  >
    <Form.Item label="Num1">
      <InputNumber value={num1} onChange={value => onChange('num1', value)} />
    </Form.Item>
    <Form.Item label="+ Num2">
      <InputNumber value={num2} onChange={value => onChange('num2', value)} />
    </Form.Item>
    <Form.Item>
      <Button type="primary" htmlType="submit">
        Calculate
      </Button>
    </Form.Item>
  </Form>
);

export default Calculator;
