import React from 'react';
import { Statistic, Button } from 'antd';

const Random = ({ onRand, value }) => (
  <div>
    <Statistic title="Random number" value={value} precision={3} />
    <Button style={{ marginTop: 16 }} type="primary" onClick={onRand}>
      Refresh
    </Button>
  </div>
);

export default Random;
