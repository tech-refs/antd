import React from 'react';
import { connect } from 'dva';
import Random from '../components/Random';

const Rand = ({ dispatch, random }) => {
  function handleRand() {
    dispatch({
      type: 'random/rand',
    });
  }

  return <Random onRand={handleRand} value={random} />;
};

export default connect(({ random }) => ({ random }))(Rand);
