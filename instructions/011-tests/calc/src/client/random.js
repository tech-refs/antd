// @flow
// import request from 'axios';

// export async function queryRandom() {
//   return request('/api/rand').then(response => response.data);
// }

import request from '../utils/request';

export async function queryRandom(): Promise<number> {
  // return request('/api/rand');
  return request(
    'http://www.random.org/integers/?num=1&min=50&max=100&col=1&base=10&format=plain&rnd=new',
    { credentials: 'omit' }
  );
}
