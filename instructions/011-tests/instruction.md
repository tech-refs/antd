
# Instruction on using umi-test (jest) with antd pro and flow

## Install "Flow" plugin for babel

```sh
cd calc
npm i --dev transform-flow-strip-types
```

## Create ".babelrc" in the project root an add plugin to it

```js
{
    "plugins": [
        "transform-flow-strip-types"
    ]
}
```

## Run tests

```sh
npm test
```